"use strict";
process.env['PATH'] = process.env['PATH'] + "/" + process.env['LAMBDA_TASK_ROOT'];
const makeThumb = require("./src/makeThumb").makeThumb;
const DEBUG = require("./src/makeThumb").DEBUG;
const AWS = require("aws-sdk");
const logger = require("winston");

/**
 * AWS sdk configs
 */
let credsParam = {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID, 
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY, 
    sessionToken: process.env.AWS_SESSION_TOKEN || null
}

let creds = new AWS.Credentials(credsParam);

/**
 * s3Params - metadata for uploading to s3
 */
const s3Params = {
    Bucket: "",
    ContentEncoding: "image/jpg",
    ACL: "public-read",
    Key: "",
    Body: null
};

/**
 * parseFileName - regexp to parse name of file from url
 */
const parseFileName = /[^/]+$/g;

/**
 * handler - is a entery point for lambda
 * @param {s3event} event - path to the video to make thumb
 * @return {Promise} Result - {Err, Data}
 */
exports.handler = async (event) => {
    try {
        const vKey = event.Records[0].s3.object.key;
        const s3Bucket = event.Records[0].s3.bucket.name;
        const s3Root = `https://s3.${event.Records[0].awsRegion}.amazonaws.com`;
        let inFile = `${s3Root}/${s3Bucket}/${vKey}`;
        if (DEBUG)
            logger.log("info", `Video file path ${inFile}`);
        let res = await makeThumb(inFile);
        if (res.Err != null)
            throw new Error(res.Err);
        AWS.config.update(new AWS.Config({
            credentials: creds, 
            region: event.Records[0].awsRegion
        }));
        const s3 = new AWS.S3();
        s3Params.Key = `${vKey.toString().split('.')[0]}_thumb.jpg`;
        s3Params.Body = res.Data.Stream;
        s3Params.Bucket = s3Bucket;
        return new Promise(r => {
            s3.upload(s3Params, err => {
                return r({
                    Err:err,
                    Data: (err == null)? "ok":null
                });
            });
        });
    } catch (err) {
        logger.log("error", err.message);
        return {
            Err: err.message
        };
    }
}