"use strict";
const ffmpeg = require("fluent-ffmpeg");
const fs = require("fs");
const path = require("path");
const uuid = require("uuid/v4");
const request = require("request");
const logger = require("winston");
const DEBUG = (process.env.DEBUG && process.env.DEBUG == "true")? true : false;
module.exports.DEBUG = DEBUG;
/**
 * makeThumb function
 * @param {string} inFile - input video file path 
 * @return {Promise}
 */
module.exports.makeThumb = async (inFile) => {
    try {
        return new Promise(r => {
            const tempVideoFilePath = path.join("/tmp", `${uuid()}.mp4`);
            const inVideoStream = fs.createWriteStream(tempVideoFilePath);
            inVideoStream.on("close", () => {
                const video = ffmpeg(tempVideoFilePath);
                video.ffprobe((err, data) => {
                    if (err)
                        return r({
                            Data: null,
                            Err: err.message
                        });
                    let duration = parseInt(data.format.duration / 2);
                    let bufferKey = path.join("/tmp", `${uuid()}.jpg`);
                    if (DEBUG) {
                        logger.log("info", `In File url ${inFile}`);
                        logger.log("info", `Duration ${data.format.duration }`);
                        logger.log("info", `Buffer image key ${bufferKey}`);
                        logger.log("info", `Buffer video key ${tempVideoFilePath}`);
                        logger.log("info", `Crop duration ${duration}`);
                    }
                    video.seek(duration);
                    video.outputOptions([
                        "-vframes 1",
                        "-q:v 2"
                    ]);
                    video.output(bufferKey);
                    video.on("end", () => {
                        fs.unlink(tempVideoFilePath, ()=>{});
                        return r({
                            Data: {
                                Stream:fs.createReadStream(bufferKey).on("close", () => fs.unlink(bufferKey, () =>{}))
                            },
                            Err:null
                        });
                    });
                    video.run();
                });
            });
            request.get(inFile).pipe(inVideoStream);
        });
    } catch (err) {
        logger.log("error", err.message);
        return {
            Data: null,
            Err: err.message
        }
    }
};